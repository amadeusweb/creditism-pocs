<?php
am_var('local', $local = startsWith($_SERVER['HTTP_HOST'], 'localhost'));

include_once 'functions.php';

bootstrap([
	'name' => 'Creditism.org',
	'byline' => 'A New Game of Life',
	'safeName' => 'commonplanet',

	'version' => [ 'id' => '6b', 'date' => '17 Dec 2022' ],

	'folder' => 'content/',
	'support_page_parameters' => true,

	'start_year' => '2022',

	'theme' => 'biz-land',

	'styles' => ['styles', 'extras'],
	'scripts' => ['content', 'engage',
		'https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min',
		'yinyang',
	],

	'no-contact-info' => true, //TODO: Change this someday?
	'email' => 'remzi@creditism.org',
	'phone' => '+18604880545',
	'address' => 'Portland,<br />Oregon, America',

	'og:image' => '%url%assets/common-planet-opengraph.png',
	'spotify' => '4AEJaxJXSDm5NPXBnimoeE', //ONE NOTE SYMPHONY, Alan Parsons

	'social' => [
		[ 'type' => 'twitter', 'link' => 'https://www.twitter.com/Common_Planet' ],
		[ 'type' => 'facebook', 'link' => 'https://www.facebook.com/commonplanetorg' ],
		[ 'type' => 'youtube', 'link' => 'https://www.youtube.com/channel/UC3hE3JvmD9tFdEyUM_IC0Iw' ],
		[ 'type' => 'reditt', 'link' => 'https://www.reddit.com/user/Creditism' ]
		/*
		[ 'type' => 'linkedin', 'link' => 'https://www.linkedin.com/in/remzi-bajrami-32607844/' ], //TODO: Linked In Group in March?
		[ 'type' => 'github', 'link' => 'https://bitbucket.org/amadeusweb/creditism/' ], //shows our transparency with content / do we want the repo private? //TODO: move to new bitbucket acount before go live
		*/
	],

	'url' => $local ? 'http://localhost/creditism-pocs/' : '//pocs.creditism.org/',

	'path' => SITEPATH,
	'no-local-stats' => true,
]);

render();
?>
