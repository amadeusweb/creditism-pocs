<div id="footer-content" class="footer-bgd" style="margin-top: 30px;">
	<div class="container">
		<h2 class="toggle-engage">toggle calls to action</h2>
		<div class="engage" style="display: none" data-to="team@creditism.org" data-cc="remzi@creditism.org" data-name="Creditism">
			<?php renderFile(SITEPATH . '/data/calls-to-action.md'); ?>
		</div>
		<!--
		<a href="https://common-planet.org/" target="_blank">
			<img src="<?php echo am_var('url'); ?>assets/commonplanet-logo.png" alt="common-planet-logo.png, 33kB" title="common-planet-logo" class="img-fluid" />
		</a>
		<br /><br />
		-->
		<p class="footer-message"><?php echo am_var('footer-message'); ?></p>
		<br /><br />
		<div class="social-links"><?php foreach(am_var('social') as $item) { ?>
			<a target="_blank" href="<?php echo $item['link']; ?>" title="<?php echo isset($item['name']) ? $item['name'] : $item['type']; ?>" class="<?php echo $item['type']; ?>"><i class="icofont-<?php echo $item['type']; ?>"></i></a><?php } ?>
		</div>
	</div>
</div>
