<?php
am_var('onDiscussClick', 'this.nextSibling.setAttribute(\'src\', this.nextSibling.getAttribute(\'data-src\')); this.nextSibling.style.display = \'block\';');


function before_render() {
	$blurb = SITEPATH . '/blurbs/' . am_var('node') . '.txt';
	if (disk_file_exists($blurb)) {
		am_var('blurb-file', $blurb);
		am_var('embed', true);
		return;
	}

	if (am_var('node') == 'present') {
		$deck = am_var('page_parameter1');

		am_var('deck', SITEPATH . '/decks/' . $deck . '.md');
		am_var('deck-name', am_var('page_parameter1'));
		am_var('embed', true);
		return;
	}

	$sections = ['book', 'discuss', 'articles']; //TODO: variable
	foreach ($sections as $slug) {
		$path = am_var('path') . '/content/' . $slug . '/';
		if (file_exists($file = $path . am_var('node') . '.md')
			|| file_exists($file = $path . am_var('node') . '.html')) {
			am_var('fol', $path);
			am_var('section', $slug);
			am_var('file', $file);
			break;
		} else if (file_exists($file = $path . am_var('node') . '.php')) {
			am_var('file', $file);
			break;
		}
	}
}

function did_render_page() {
	if (am_var('blurb-file')) {
		load_amadeus_module('blurbs');
		return true;
	}

	if (am_var('deck')) {
		if (am_var('page_parameter2') == 'embed')
			am_var('no-detail-link', true);

		load_amadeus_module('revealjs');
		return true;
	}

	if ($section = am_var('section')) {
		render_txt_or_md(am_var('file'));
		return true;
	} else if (am_var('file')) {
		include_once am_var('file');
		return true;
	}

	return false;
}


am_var('video-template', '<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/%videoid%" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>');

function before_file() {
	if (am_var('embed')) return;

	echo '<hr class="above-header-content" />' . am_var('nl');
/*
	echo '<div id="pre-content-wrapper" class="header-bgd">' . am_var('nl');

	echo '  <header id="pre-content" class="container no-speakable-item-underline">' . am_var('nl');

	echo '    <div class="page-heading">' . am_var('nl');

	$pages = []; //TODO: SEO
	$pageName = strip_hyphens(am_var('node'));
	$page = isset($pages[$pageName]) ? $pages[$pageName] : [ 'title' => ucwords($pageName), 'description' => '...Description...' ];

	echo '      <h1 class="page-name">' . $page['title'] . '</h1>' . am_var('nl');

	//#2 - description
	echo sprintf('      <p class="page-description">%s</p>' . am_var('nl'), $page['description']);

	echo '    </div>' . am_var('nl');

	echo '  </header>' . am_var('nl');
	echo '</div>' . am_var('nl');

	include 'header-content.php';
	echo '<hr class="page-heading-separator" />';
*/

	echo '<div id="content" class="container">';
/*
	$deckExists = file_exists(SITEPATH . '/decks/' . am_var('node') . '.md');
	if ($deckExists)
		echo sprintf('<div class="deck-container"><iframe src="%spresent/%s/embed/"></iframe></div>', am_var('url'), am_var('node'));
*/
}

function after_file() {
	if (am_var('embed')) return;
	echo '</div>';
}

function item_r($col, $item, $return = false) {
	$cols = am_var('sectionColumns');

	$r = $item[$cols[$col]];

	$r = str_replace('|', '<br />', $r);
	$r = simplify_encoding($r);
	$r = replace_vars($r);
	$r = str_replace('<a href', '<a target="_blank" href', $r);
	if ($return) return $r;

	echo $r;
}

?>
