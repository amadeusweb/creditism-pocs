<?php
if (false && array_search(am_var('node'), [
		//portrait + horizontal resolution based banners
		'index',
		'read-full',
	]) !== false) { ?>
	<div>
		<img src="<?php echo am_var('url'); ?>assets/pages/<?php echo am_var('node');?>-portrait.jpg" class="img-fluid show-in-portrait" />
		<img src="<?php echo am_var('url'); ?>assets/pages/<?php echo am_var('node');?>.jpg?fver=2" class="img-fluid show-in-landscape" />
	</div>
<hr />
<?php } else if (array_search(am_var('node'), [
		//single image horizontal only page banners
	]) !== false) { ?>
	<div><img src="<?php echo am_var('url'); ?>assets/pages/<?php echo am_var('node');?>.jpg" class="img-fluid" /></div>
<hr />
<?php } ?>
