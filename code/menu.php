<ul class="nav-menu">
<li class="drop-down"><a>Demo Features</a>
<ul>
	<li><a href="<?php echo am_var('url'); ?>just-money/" target="_blank">Money Blurbs</a>
	<li class="drop-down"><a>Read</a>
		<?php menu('/content/articles/'); ?>
	</li>
	<li class="drop-down"><a>Understand</a>
		<ul>
			<li><a href="<?php echo am_var('url'); ?>summary/">Summary</a></li>
			<li><a href="<?php echo am_var('url'); ?>glossary/"><u>Glossary</u></a></li>
			<?php menu('/content/definitions/', ['parent-slug' => 'glossary/#', 'no-ul' => true]); ?>
		</ul>
	</li>
	<li class="drop-down"><a>Discuss</a>
		<?php menu('/content/discuss/'); ?>
	</li>
	<li class="drop-down"><a>Get Involved</a>
		<ul>
			<li><a href="<?php echo am_var('url'); ?>act-with-us/">Act with Us</a></li>
			<li><a href="<?php echo am_var('url'); ?>interact/">Interact with Us</a></li>
			<li><a href="https://common-planet.org/donate/" target="_blank">Donate to Common Planet</a></li>
			<li><a href="https://community.common-planet.org/" target="_blank">Visit the Community</a></li>
		</ul>
	</li>
	<li class="drop-down"><a>Book</a>
		<ul>
			<li><a href="<?php echo am_var('url'); ?>the-book/">About the Book</a></li>
			<li><a href="<?php echo am_var('url'); ?>read-pdf/">Read the PDF Online</a></li>
			<li><a href="<?php echo am_var('url'); ?>read-full/">Read Online</a></li>
			<li><a href="https://www.amazon.com/dp/0578381087" target="_blank">Order on Amazon</a></li>
		</ul>
	</li>
</ul>
</ul>
