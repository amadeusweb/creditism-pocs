<hr />
<a class="btn btn-lg btn-primary" href="https://docs.google.com/spreadsheets/d/101Rniqol49OlEK148xvZg8RlmraN-7wmdbyUhrpECKs/edit#gid=0" target="_blank">INFOGRAPHICS IS FROM THIS COMPARISON SHEET:</a><br><br>

<table class="compare">
<tr>
<th>What</th>
<th>Titles A / B</th>
</tr>
<?php
$data = get_sheet('comparisons', false);
foreach ($data->rows as $item) {
	if (item_r('What', $item, true) == '__') {
		echo '<tr class="superheaderx2"><td></td><th colspan="4">'; item_r('TitleA', $item);
		echo '</th></tr>';
		continue;
	}

	echo '<tr class="superheader toggle"><th>'; item_r('What', $item);
		echo '</th><th><span class="toggle">+</span>'; item_r('TitleA', $item);
		echo ' <span class="versus">[versus]</span> '; item_r('TitleB', $item);
	echo '</th></tr>';
	echo '<tr class="hide"><td colspan="2"><strong>'; item_r('TitleA', $item);
		echo '</strong><br />'; item_r('DescriptionA', $item);
	echo '</td></tr><tr class="hide"><td colspan="2"><strong>'; item_r('TitleB', $item);
		echo '</strong><br />'; item_r('DescriptionB', $item);
	echo '</td></tr>';
}
?>
</table>
<style type="text/css">
.superheader, .compare strong { background-color: #BBE3FE; }
.compare { background-color: #B4C5EB; width: 100%; }
.compare tr.toggle { border-top:  2px solid purple; }
.compare th, .compare td { padding: 6px; font-size: 140%; }
.compare .versus { background-color: yellow; }
.compare .hide { display: none; }
.compare span.toggle { font-size: 140%; margin-left: 10px; float: right; line-height: 30px; }
</style>
