<?php
echo '<h2>By Group</h2>';
echo '<a href="' . am_var('url') . 'act-with-us/">Main Group</a>';
menu('/content/actions/', ['parent-slug' => 'act-with-us/']);
echo '<hr />';

$group = am_var('page_parameter1') ? am_var('page_parameter1') : 'index';

$item = [ 'email' => 'future@common-planet.org' ]; //TODO: address to Imran's mailbox, cc to remzi


if ($group) {
	echo '<h1>Group: ' . humanize($group) . '</h1><hr />';
	echo '<div class="engage" data-to="' . $item['email'] . '" data-cc="team@yieldmore.org" data-name="Common Planet Action - ' . humanize($group) . '">';
	render_txt_or_md(SITEPATH . '/content/actions/' . $group . '.md');
	echo '</div>';
}

echo '<hr />';
render_txt_or_md('# Choose one of the groups of activities to act with us

Do read [Our Terms]([url]terms/) to understand us an our intents fully before offering support.

When you reach out to your readers and volunteers, encourage them to track their activity on the lightworker specific "enthusiast" form (listed below) so we all know who is being helped and how.

You may [subscribe to our group](https://groups.io/g/alliances-for-a-golden-world) and then send an introduction email to [alliances@yieldmore.org](mailto:alliances@yieldmore.org).
');
