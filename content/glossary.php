<div id="glossary">
<?php
$items = get_sheet('glossary', false);
foreach ($items->rows as $item) {
	$hasDefinition = item_r('definition', $item, true) !== '';
	$name = item_r('name', $item, true);
	echo sprintf('<a name="%s/"></a><h3%s>%s</h3><p>%s</p>%s',
		urlize($name),
		$hasDefinition ? ' class="has-definition"' : '',
		$name,
		item_r('description', $item, true),
		$hasDefinition ? '<div class="definition" style="display: none;">' . render_txt_or_md(SITEPATH . '/content/definitions/' . urlize($name) . '.md', [], false) . '</div>' : ''
	);
}
?>
</div>