Creditism is the economics of the future and here is how you can get involved. You could also join the discussion on facebook / linkedin and refer us to other like minded friends. Lastly, any donation you make would be much appreciated.

# Book Related

* Bought the Book
* Disagreed with the Book
* Referred the Book
* Made a Video about the Book

# Collaboration

* Help us improve the concept, its verbiage and visual presentation
* Share in your circles
* Create Conferences with other forward thinkers and quasi monetary communities.

# Hosts and Promoters

* Spoke to someone with a podcast
* Organized a meeting to discuss the concept

# At my Organization

* Promoted the Idea of Monthly Support
* Monthly Get togethers to discuss how to effect Creditism

# Volunteering

* Will do promotion and crowdfunding
* Want to expand the ideas
* Help with Networking and Social Media Moderation
* Video Content and Speaking Engagements
